# kiapan project

A different repo for the kiapan project since the first one is... dead
to say the least.


## building

The server and client rely on several node libraries, and are both implemented in
typescript. Running `yarn` and `yarn start` is enough to start up the development
server. The server is hosted on `:8080` by default, but changing one line in
`src/kiapan.ts` is enough to change that to whatever you'd like.

## configuration

The `PORT` and `GFC_SECRET` environmental variables can be used. This
is useful for deployment as vars can usually be configured in the server or container.


## features implemented

- descriminators
- mentioning `user@descrim`
- send and receive messages
- enforce login for sending messages
- enforce login for viewing messages on official client
  - this could easily be reverse engineered by copying the /js code but still
- save login info in cookies
